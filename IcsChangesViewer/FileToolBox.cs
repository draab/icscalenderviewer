﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IcsChangesViewer
{
    /// <summary>
    /// contains static functions for handling Files
    /// </summary>
    class FileToolBox
    {
        /// <summary>
        /// converts a String path to DirectoryInfo object and checks if the folder exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns>DirectoryInfo object corresponding to the given string</returns>
        /// <exception cref="DirectoryNotFoundException"> is thrown if path does not exist</exception>
        public static DirectoryInfo GetDirectoryInfoFromString (String path)
        {
            DirectoryInfo folder = new DirectoryInfo(path);

            if (folder.Exists)
                return folder;

            throw new DirectoryNotFoundException("Directory is not valid. Please try again.");
        }

        /// <summary>
        /// generates a list of CalendarManager objects from the files in the given folder;
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public static List<CalendarManager> GetCalendarListFromFolder(DirectoryInfo folder)
        {
            //use set for getting calendar name once for multiple versions
            Dictionary<string, CalendarManager> calDict = new Dictionary<string, CalendarManager>();

            //iterate all files in given folder
            foreach(FileInfo info in folder.GetFiles())
            {
                try {
                    //if the filename is not valid, the constructor will throw an exception
                    CalendarManager.CalendarVersion cv = new CalendarManager.CalendarVersion(info);

                    //if calendarname is allready in the dictonary add version to the calendarManager in the dict
                    if(calDict.ContainsKey(cv.CalendarName)) {
                        calDict[cv.CalendarName].AddVersion(cv);
                    } else {
                        //if calendarname not in the dict, add new calendarManager with version in it to the dict
                        CalendarManager c = new CalendarManager(cv.CalendarName);
                        c.AddVersion(cv);
                        calDict[c.Name] = c;
                    }
                } catch(Exception) {
                    // nothing to do, skip file
                }
            }

            return calDict.Values.ToList();
        }
    }
}
