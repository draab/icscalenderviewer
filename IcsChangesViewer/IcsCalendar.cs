using System.IO;
using System.Collections.Generic;

namespace IcsChangesViewer
{
    /// <summary>
    /// represents and contains methods for parsing an ics file;
    /// all after BEGIN:VCALENDAR till END:VCALENDAR
    /// </summary>
    public class IcsCalendar {

        private List<IcsEvent> _eventList = new List<IcsEvent>();
        public string Version { get; private set; }
        public string ProdId { get; private set; }
        public string Method { get; private set; }
        
        public bool EndFound { get; private set; } //show if end was found; not used at the moment
        public string UnknownLines {get; private set;} //info not taken from the read functions will stored here
        
        public void AddEvent(IcsEvent icsEvent) {
            _eventList.Add(icsEvent);   //rapped for later development (some checks bevor adding)
        }

        public List<IcsEvent> EventList {
            get {
                return _eventList;
            }
        }

        private IcsCalendar() {}

        /// <summary>
        /// parse given file to IcsCalendar obj
        /// </summary>
        /// <param name="icsFile"></param>
        /// <returns></returns>
        public static IcsCalendar ParseIcsFromStream(FileInfo icsFile) {
            string line;

            StreamReader stream = new StreamReader(icsFile.OpenRead());

            //normaly this loop should only have one run
            while ((line = stream.ReadLine()) != null) {

                //their should be only one VCALENDAR per file
                if (line.StartsWith("BEGIN:VCALENDAR")) {
                    // parse calendar
                    return ParseVCalendar(stream);
                }
            }
            return null;
        }

        /// <summary>
        /// parses all lines of the stream to an icsCalendar;
        /// all after BEGIN:VCALENDAR till END:VCALENDAR
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns></returns>
        private static IcsCalendar ParseVCalendar(StreamReader streamReader) {
            IcsCalendar cal = new IcsCalendar();
            cal.EndFound = false;
            
            string line;

            while((line = streamReader.ReadLine()) != null) {

                if(line.StartsWith("VERSION:")) {
                    cal.Version = line.Split(':', 2)[1];
                } else if(line.StartsWith("PRODID:")) {
                    cal.ProdId = line.Split(':', 2)[1];
                } else if(line.StartsWith("METHOD:")) {
                    cal.Method = line.Split(':', 2)[1];
                } else if(line.StartsWith("BEGIN:VEVENT")) {
                    cal.AddEvent(IcsEvent.ParseVEvent(streamReader));
                } else if(line.StartsWith("END:VCALENDAR")) {
                    cal.EndFound = true;
                    return cal;
                } else {
                    cal.UnknownLines += line + " - ";   //to find out uncatched lines
                }
            }
            return cal;
        }

        /// <summary>
        /// represents and contains methods for parsing vEvent in File (stream);
        /// all after BEGIN:VEVENT till END:VEVENT
        /// </summary>
        public class IcsEvent
        {
            public bool EndFound {get; private set;}
            public string UnknownLines {get; private set;}
            public string UID {get; private set;}
            public string Location {get; private set;}
            public string Summary {get; private set;}
            public string Description {get; private set;}
            public string Class_ {get; private set;}
            public string Start {get; private set;} //TODO change to DateTime
            public string End {get; private set;}   //TODO change to DateTime
            public string Stamp {get; private set; }   //TODO change to DateTime

            /// <summary>
            /// enumaration corresponding all Information in IcsEvent class
            /// </summary>
            public enum Information : byte {
                UID=1,
                Location=2,
                Summary=4,
                Description=8,
                Class_=16,
                Start = 32,
                End = 64,
                Stamp = 128
            }

            private IcsEvent() {}

            /// <summary>
            /// parses all lines of the stream to an icsEvent obj;
            /// all after BEGIN:VEVENT till END:VEVENT
            /// </summary>
            /// <param name="streamReader"></param>
            /// <returns></returns>
            public static IcsEvent ParseVEvent(StreamReader streamReader) 
            {
                IcsEvent e = new IcsEvent();
                e.EndFound = false;
                
                string line;

                while((line = streamReader.ReadLine()) != null) {

                    //check read line, and fill attributes with content
                    if(line.StartsWith("UID:")) {
                        e.UID = line.Split(':', 2)[1];
                    } else if(line.StartsWith("LOCATION:")) {
                        e.Location = line.Split(':', 2)[1];
                    } else if(line.StartsWith("SUMMARY:")) {
                        e.Summary = line.Split(':', 2)[1];
                    } else if(line.StartsWith("DESCRIPTION")) {
                        e.Description = line.Split(';', 2)[1];
                        if (e.Description.StartsWith("ENCODING=QUOTED-PRINTABLE:"))
                            e.Description = e.Description.Split(':',2)[1];
                        e.Description = e.Description.Replace("=0D=0A", "");
                    } else if(line.StartsWith("CLASS:")) {
                        e.Class_ = line.Split(':', 2)[1];
                    } else if(line.StartsWith("DTSTART:")) {
                        e.Start = line.Split(':', 2)[1];
                    } else if(line.StartsWith("DTEND:")) {
                        e.End = line.Split(':', 2)[1];
                    } else if(line.StartsWith("DTSTAMP:")) {
                        e.Stamp = line.Split(':', 2)[1];
                    } else if(line.StartsWith("END:VEVENT")) {
                        e.EndFound = true;
                        return e;
                    } else {
                        //all other attributes from ics file will be stored here
                        e.UnknownLines += line + " ; ";   //to find out uncatched lines
                    }
                }

                //should not be reached
                return e;
            }

            public bool HasDifferences(IcsEvent e) {
                //dont care about stamp differences
                return (GetDifferences(e) & ~Information.Stamp) != 0;
            }

            /// <summary>
            /// checks all Attributes from this object against the given icsEvent obj
            /// </summary>
            /// <param name="e"></param>
            /// <returns>Information, representing all Attributes which are not equal</returns>
            public Information GetDifferences(IcsEvent e) {

                Information diff = 0;

                //comparing each Attribute with Attribute of given e;
                //add "flag" to Information byte if attributes not equal
                if(! e.UID.Equals(this.UID))
                    diff |= Information.UID;
                if(! e.Location.Equals(this.Location))
                    diff |= Information.Location;
                if(! e.Summary.Equals(this.Summary))
                    diff |= Information.Summary;
                if(! e.Description.Equals(this.Description))
                    diff |= Information.Description;
                if(! e.Class_.Equals(this.Class_))
                    diff |= Information.Class_;
                if(! e.Start.Equals(this.Start))
                    diff |= Information.Start;
                if(! e.End.Equals(this.End))
                    diff |= Information.End;
                if(! e.Stamp.Equals(this.Stamp))
                    diff |= Information.Stamp;

                // each bit which is 1, shows an difference
                return diff;
            }
        }
    }
}