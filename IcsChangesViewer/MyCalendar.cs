using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace IcsChangesViewer
{
    /// <summary>
    /// represents a calendar, which (can) have multiple versions
    /// a version is a ics calendar from a specific date
    /// </summary>
    class CalendarManager {

        private List<CalendarVersion> _versionList = new List<CalendarVersion>();

        /// <summary>
        /// name of the calendar, comes from the filename
        /// </summary>
        public string Name { get; set; }

        public CalendarManager(string name) {
            this.Name = name;
        }

        /// <summary>
        /// adds a version on a specific date in the list of this calendar
        /// </summary>
        /// <param name="version"></param>
        public void AddVersion(CalendarVersion version) {
            _versionList.Add(version);
        }

        public List<CalendarVersion> VersionList {
            get { return _versionList; }
        }
        
        /// <summary>
        /// is a version of an Calendar;
        /// it contains a IcsCalendar object, which correnspond to one ics file
        /// </summary>
        public class CalendarVersion
        {
            /// <summary>
            /// default datetime format string for the given files
            /// </summary>
            public static string DefaultDateFormatString = "yyyy-MM-dd_HH-mm-ss";

            public string CalendarName { get; private set; }
            private IcsCalendar _versionContent = null;
            public IcsCalendar VersionContent { 
                get {
                    if (_versionContent == null)
                        _versionContent = IcsCalendar.ParseIcsFromStream(IcsFile);
                    return _versionContent;
                }
            }
            public FileInfo IcsFile { get; }
            public DateTime Date { get; private set; }

            public CalendarVersion(FileInfo file) {
                this.IcsFile = file;

                parseCalendarNameAndDate();
            }

            /// <summary>
            /// try to parse calendarname and version-date from filename
            /// </summary>
            /// <exception cref="InvalidCastException">will be thrown if filename cannot be parsed</exception>
            /// <exception cref="FormatException">will be thrown if filename cannot be parsed</exception>
            /// <exception cref="ArgumentNullException">will be thrown if filename cannot be parsed</exception>
            private void parseCalendarNameAndDate()
            {
                string filename = this.IcsFile.Name;

                int delemiterPos = filename.IndexOf('_'); //delemits calendar name and date of version
                int fileExtPos = filename.IndexOf('.'); //delemits file extention

                //check if filename correspond to format
                if (delemiterPos > 0 && fileExtPos > 0)
                {
                    //first part is calendar name
                    this.CalendarName = filename.Substring(0, delemiterPos);

                    // second part without fileExtention is the date and time
                    String dateStr = filename.Substring(delemiterPos+1, fileExtPos- delemiterPos-1);

                    //ParseExact throws Exception if date is not in correct format
                    this.Date = DateTime.ParseExact(dateStr, DefaultDateFormatString, CultureInfo.InvariantCulture);
                } else {
                    throw new InvalidCastException();
                }
            }

            /// <summary>
            /// The method GetNewEventList is callend, if the list is empty return false, otherwise true
            /// </summary>
            /// <param name="last"></param>
            /// <returns>returns true if the called method GetNewEventList contains elements</returns>
            public bool HasNewEvent (CalendarVersion last) {
                return GetNewEventList(last).Count > 0;
            }

            /// <summary>
            /// search every event from this calendar version obj in the given last calendarversion;
            /// if the event is not found it is a new one and will return in the list;
            /// Returns a relative complememnt (Differenzmenge, ohne) - (this events) \ (last last)
            /// </summary>
            /// <param name="last"></param>
            /// <returns></returns>
            public List<IcsCalendar.IcsEvent> GetNewEventList (CalendarVersion last) {
                List<IcsCalendar.IcsEvent> eList = new List<IcsCalendar.IcsEvent>();
                
                // iterate over events in current calendar version
                foreach(IcsCalendar.IcsEvent e in VersionContent.EventList) {

                    //looking if an event from current calendar version is in last calendar version
                    if(! last.VersionContent.EventList.Exists(x => x.UID.Equals(e.UID))) {
                        // if an event is not in last calendar version it must be a new one
                        eList.Add(e);
                    }
                }

                return eList;
            }

            /// <summary>
            /// The method calls GetChangedEventList; if the returned list is empty, false will be returned, otherwise true
            /// </summary>
            /// <param name="cv"></param>
            /// <returns>returns true if there are changed events</returns>
            public bool HasChangedEvent (CalendarVersion cv) {
                return GetChangedEventList(cv).Count > 0;
            }

            /// <summary>
            /// returns list of changed events (events which has differences;
            /// if a event is only in one CalendarVersion it will not be listed
            /// </summary>
            /// <param name="cv"></param>
            /// <returns>Events which are in both CalendarVisions and have differences are return as List</returns>
            public List<IcsCalendar.IcsEvent> GetChangedEventList (CalendarVersion cv) {
                List<IcsCalendar.IcsEvent> eList = new List<IcsCalendar.IcsEvent>();

                foreach(IcsCalendar.IcsEvent e in VersionContent.EventList) {
                    if(cv.VersionContent.EventList.Exists(x => x.UID.Equals(e.UID) && x.HasDifferences(e))) {
                        eList.Add(e);
                    }
                }

                return eList;
            }
        }
    }
}