﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace IcsChangesViewer
{
    class ConsoleManager
    {

        private static ConsoleColor successColor = ConsoleColor.Green;
        private static ConsoleColor warningColor = ConsoleColor.Yellow;
        private static ConsoleColor errorColor = ConsoleColor.Red;

        static ConsoleManager()
        {
            Console.WriteLine("#########################################################");
            Console.WriteLine("############     Calendar Change Viewer     #############");
            Console.WriteLine("#########################################################\n");
            Console.WriteLine("Exit the application with 'Ctrl' + 'c'\n");
        }

        public static string PollDirectory()
        {
            Console.WriteLine("Please enter a valid directory:");
            Console.Write("-> ");

            return Console.ReadLine();
        }

        public static void ShowWarning(String message)
        {
            Console.ForegroundColor = warningColor;
            Console.WriteLine(message);
            Console.Beep();
            Console.ResetColor();
            Console.WriteLine();
        }

        public static void ShowCalendarList(List<CalendarManager> list) {

            // check if list contains elements
            if(list.Count == 0) {
                throw new ArgumentException("list contains no element.");
            }

            // sentence for intro
            Console.WriteLine("\nThe folder consists of following calendars:");
            
            //list calendars or text if no entries
            for(int i=0; i<list.Count; i++) {
                Console.ForegroundColor = successColor;
                Console.Write("{0}", i+1);
                Console.ResetColor();
                Console.WriteLine(" - {0} --- stored versions: {1}", list[i].Name, list[i].VersionList.Count);
            }


            //show possible choices
        }
        public static CalendarManager ReadCalendarChoice(List<CalendarManager> list) {

            // check if list contains elements
            if(list.Count == 0) {
                throw new ArgumentException("list contains no element.");
            }

            // sentence for intro
            Console.Write("Choose a calendar [1-{0}]: ", list.Count);
            
            // read integer for calendar from console
            string strChoice = Console.ReadLine();
            int choice = int.Parse(strChoice);

            if(choice < 1 || choice > list.Count)
                throw new ArgumentException();

            return list[choice-1];
        }

        public static void ListCalendarVersions(CalendarManager calendar) {

            // check if calendar contains versions, VersionsCount should not be able to be 0
            if(calendar.VersionList.Count == 0) {
                throw new ArgumentException("calendar contains no version.");
            }

            // sentence for intro
            Console.Write("All versions of - ");
            Console.ForegroundColor = successColor;
            Console.WriteLine("{0}", calendar.Name);
            Console.ResetColor();

            //sort list for comparing to the last one
            calendar.VersionList.Sort(delegate (CalendarManager.CalendarVersion a, CalendarManager.CalendarVersion b) {
                return a.Date.CompareTo(b.Date);
            });
            
            //list calendars or text if no entries
            //foreach(CalendarManager.CalendarVersion cv in calendar.VersionList) {
            for(int i=0; i < calendar.VersionList.Count; i++) {
                CalendarManager.CalendarVersion currentCV = calendar.VersionList[i];
                if(i == 0)
                    Console.Write("  -  ---");
                else {
                    CalendarManager.CalendarVersion lastCV = calendar.VersionList[i-1];

                    Console.Write("{0,3}  ", i+1);
                    if (lastCV.HasNewEvent(currentCV)) {
                        Console.ForegroundColor = warningColor;
                        Console.Write('R');     // R ... removed
                        Console.ResetColor();
                    } else {
                        Console.Write('-');
                    }
                    if (currentCV.HasChangedEvent(lastCV)) {
                        Console.ForegroundColor = errorColor;
                        Console.Write('C');     //C .... changed
                        Console.ResetColor();
                    } else {
                        Console.Write('-');
                    }
                    if (currentCV.HasNewEvent(lastCV)) {
                        Console.ForegroundColor = successColor;
                        Console.Write('N');     //N ... new
                        Console.ResetColor();
                    } else {
                        Console.Write('-');
                    }
                }
                Console.Write("  {0}", currentCV.Date.ToString("ddd, dd MMMM yyyy - HH:mm:ss"));
                Console.WriteLine("  -  Events: {0}", currentCV.VersionContent.EventList.Count);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="calendar"></param>
        /// <returns>null if back is pressed, the curresponded CalendarVersion outerwise</returns>
        /// <exception cref="ArgumentException">if choice is out of range</exception>
        public static CalendarManager.CalendarVersion ReadCalendarVersionChoice(CalendarManager  calendar) {

            // sentence for intro
            Console.Write("Type in versionnumber to show more details [2-{0}; b]: ", calendar.VersionList.Count);
            
            // read integer for calendar from console
            string strChoice = Console.ReadLine();

            //if back is pressed null will returned
            if(BackPressed(strChoice))
                return null;

            int choice = int.Parse(strChoice);

            //if choice is out of range throw argurment exception
            if(choice < 2 || choice > calendar.VersionList.Count)
                throw new ArgumentException();

            return calendar.VersionList[choice-1];
        }

        public static void ListVersionDifferences(CalendarManager calendar, CalendarManager.CalendarVersion calendarVersion) {

            
            // sentence for intro
            Console.Write("Calendar ");
            Console.ForegroundColor = successColor;
            Console.Write("{0}", calendar.Name);
            Console.ResetColor();
            Console.Write(", Version from: ");
            Console.ForegroundColor = warningColor;
            Console.Write("{0}", calendarVersion.Date.ToString("ddd, dd MMMM yyyy - HH:mm:ss"));
            Console.ResetColor();
            Console.WriteLine(":");

            //get prev version
            
            //calendar.VersionList.Sort((x,y)=> x.Date.CompareTo(y.Date)); //not neccessayr, list is already sorted and not manipulated
            int curId = calendar.VersionList.IndexOf(calendarVersion);
            if(curId < 1)
                throw new Exception("Illegal State, this should never happen. Selected Version is not comparable!");
            
            CalendarManager.CalendarVersion lastCV = calendar.VersionList[curId-1];


            //display removed:
            List<IcsCalendar.IcsEvent> removedList = lastCV.GetNewEventList(calendarVersion);
            Console.ForegroundColor = errorColor;
            for(int i=0; i<removedList.Count; i++) {
                Console.WriteLine(" - {0}; {1}; {2}", removedList[i].Start, removedList[i].Location, removedList[i].Summary);
            }
            Console.ResetColor();


            //list versions and display changes:
            List<IcsCalendar.IcsEvent> changedList = calendarVersion.GetChangedEventList(lastCV);
            for(int i=0; i<changedList.Count; i++) {
                IcsCalendar.IcsEvent newEvent = changedList[i];
                IcsCalendar.IcsEvent oldEvent = lastCV.VersionContent.EventList.Find(x => x.UID.Equals(newEvent.UID));

                IcsCalendar.IcsEvent.Information diffInfo = newEvent.GetDifferences(oldEvent);

                Console.WriteLine("\n ~ {0}", newEvent.Start);
                if((diffInfo & IcsCalendar.IcsEvent.Information.Start) != 0) {
                    Console.ForegroundColor = errorColor;
                    Console.WriteLine("     {0}", oldEvent.Start);
                    Console.ForegroundColor = successColor;
                    Console.WriteLine("     {0}", newEvent.Start);
                }
                if((diffInfo & IcsCalendar.IcsEvent.Information.Summary) != 0) {
                    Console.ForegroundColor = errorColor;
                    Console.WriteLine("     {0}", oldEvent.Summary);
                    Console.ForegroundColor = successColor;
                    Console.WriteLine("     {0}", newEvent.Summary);
                }
                if((diffInfo & IcsCalendar.IcsEvent.Information.Description) != 0) {
                    Console.ForegroundColor = errorColor;
                    Console.WriteLine("     {0}", oldEvent.Description);
                    Console.ForegroundColor = successColor;
                    Console.WriteLine("     {0}", newEvent.Description);
                }

                Console.ResetColor();
            }   
            Console.WriteLine();

            //display added:
            List<IcsCalendar.IcsEvent> newList = calendarVersion.GetNewEventList(lastCV);
            Console.ForegroundColor = successColor;
            for(int i=0; i<newList.Count; i++) {
                Console.WriteLine(" + {0}; {1}; {2}", newList[i].Start, newList[i].Location, newList[i].Summary);
            }
            Console.ResetColor();


            if(newList.Count == 0 && changedList.Count == 0 && removedList.Count == 0) {
                Console.WriteLine("  -- No changes --\n");
            }

        }

        
        public static void ReadVersionDifferenceChoices() {

            // sentence for intro
            Console.Write("Press 'b' to go back: ");
            
            // read integer for calendar from console
            string strChoice = Console.ReadLine();

            if(!BackPressed(strChoice))
                throw new ArgumentException();
        }




        private static bool BackPressed(string str) {
            return str.Trim().Equals("b");
        }
    }
}
