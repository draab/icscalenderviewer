﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace IcsChangesViewer
{
    /// <summary>
    /// Contains the Main of the Application and represents the a statemachine
    /// </summary>
    class Program
    {
        private enum State
        {
            PullDirectory=0,
            ChooseCalendar=1,
            ListCalendarVersions=4,
            ListVersionDifferences=6,
            Exit=-1
        }


        static void Main(string[] args)
        {
            State curState = State.PullDirectory;

            DirectoryInfo dirInfo = null;
            List<CalendarManager> calenderList = new List<CalendarManager>();
            CalendarManager choosenCalendar = null;
            CalendarManager.CalendarVersion choosenCalendarVersion = null;

            while (curState != State.Exit) {
                switch(curState)
                {
                    // propt a directory pull and try to get the directoryInfo object from it
                    case State.PullDirectory:
                        try
                        {
                            String dirStr = ConsoleManager.PollDirectory();
                            dirInfo = FileToolBox.GetDirectoryInfoFromString(dirStr);
                            curState = State.ChooseCalendar;
                        } catch (Exception e) when(e is IOException || e is ArgumentException) {
                            ConsoleManager.ShowWarning(e.Message);
                        }
                        break;

                    case State.ChooseCalendar:
                        try
                        {
                            calenderList = FileToolBox.GetCalendarListFromFolder(dirInfo);
                            if(calenderList.Count == 0) {
                                ConsoleManager.ShowWarning("Folder contains no calendar.\n     Calendar must have following format: <calendarname>_yyyy-MM-dd_HH-mm-ss.ics");
                                curState = State.PullDirectory;
                            } else {
                                ConsoleManager.ShowCalendarList(calenderList);
                                choosenCalendar = ConsoleManager.ReadCalendarChoice(calenderList);
                                curState = State.ListCalendarVersions;
                            }
                        }
                        catch (IOException)
                        {
                            curState = State.PullDirectory;
                            ConsoleManager.ShowWarning("Directory is not valid.");
                        }
                        catch (Exception e) when (e is ArgumentException || e is FormatException)
                        {
                            ConsoleManager.ShowWarning("Input is not valid.");
                        }
                        break;


                    case State.ListCalendarVersions:
                        try {
                            ConsoleManager.ListCalendarVersions(choosenCalendar);
                            choosenCalendarVersion = ConsoleManager.ReadCalendarVersionChoice(choosenCalendar);
                            if(choosenCalendarVersion == null) {
                                curState = State.ChooseCalendar;
                            } else {
                                curState = State.ListVersionDifferences;
                            }
                        } catch(Exception e) when(e is ArgumentException || e is FormatException) {
                            ConsoleManager.ShowWarning("Input is not valid.");
                        }
                        break;

                    case State.ListVersionDifferences:
                        try {
                            ConsoleManager.ListVersionDifferences(choosenCalendar, choosenCalendarVersion);

                            //TODO implement ShowEvent
                            ConsoleManager.ReadVersionDifferenceChoices();
                            curState = State.ListCalendarVersions;
                        } catch(ArgumentException) {
                            ConsoleManager.ShowWarning("Input is not valid.");
                        } catch(Exception e) {
                            ConsoleManager.ShowWarning(e.Message);
                        }
                        break;

                    default:
                        curState = State.Exit;
                        break;
                }
            }
        }
    }
}